import { parse } from "./parser";

describe("Parsing Notcl", () => {
  describe("Commands", () => {
    it("can parse an empty script", () =>
      expect(parse("")).toEqual([true, []]));

    it("can parse a multi-word command", () =>
      expect(parse("a b  c")).toEqual([
        true,
        [
          [
            { bare: "a", pos: 0 },
            { bare: "b", pos: 2 },
            { bare: "c", pos: 5 },
          ],
        ],
      ]));

    it("accepts newlines as command separators", () =>
      expect(parse("a\nb")).toEqual([
        true,
        [[{ bare: "a", pos: 0 }], [{ bare: "b", pos: 2 }]],
      ]));
    it("does not split commands on folded newlines", () =>
      expect(
        parse(String.raw`a\
b`)
      ).toEqual([
        true,
        [
          [
            { bare: "a", pos: 0 },
            { bare: "b", pos: 3 },
          ],
        ],
      ]));
    it("does split words on folded newlines", () =>
      expect(
        parse(String.raw`a\
b`)
      ).toEqual([
        true,
        [
          [
            { bare: "a", pos: 0 },
            { bare: "b", pos: 3 },
          ],
        ],
      ]));
    it("does split commands on newlines with escaped backslashes", () =>
      expect(
        parse(String.raw`a\\
b`)
      ).toEqual([true, [[{ text: "a\\", pos: 0 }], [{ bare: "b", pos: 4 }]]]));
    it("does not split commands on folded newlines with escaped backslashes", () =>
      expect(
        parse(String.raw`a\\\
b`)
      ).toEqual([
        true,
        [
          [
            { text: "a\\", pos: 0 },
            { bare: "b", pos: 5 },
          ],
        ],
      ]));

    it("accepts semicolons as command separators", () =>
      expect(parse("a;b")).toEqual([
        true,
        [[{ bare: "a", pos: 0 }], [{ bare: "b", pos: 2 }]],
      ]));

    it("tolerates, and ignores, empty commands", () =>
      expect(parse("a;;b\n\nc")).toEqual([
        true,
        [
          [{ bare: "a", pos: 0 }],
          [{ bare: "b", pos: 3 }],
          [{ bare: "c", pos: 6 }],
        ],
      ]));

    test.each([
      [" a", 1],
      ["a ", 0],
      ["a ;", 0],
      ["; a", 2],
    ])("tolerates whitespace before and after commands {%s}", (text, pos) =>
      expect(parse(text)).toEqual([true, [[{ bare: "a", pos }]]])
    );
  });

  describe("Comments", () => {
    it("ignores comments", () => expect(parse("#comment")).toEqual([true, []]));
    it("does not treat # in argument position as a comment", () =>
      expect(parse("a #1")).toEqual([
        true,
        [
          [
            { bare: "a", pos: 0 },
            { bare: "#1", pos: 2 },
          ],
        ],
      ]));
    it("can have commands before a comment", () =>
      expect(parse("a ;#comment")).toEqual([true, [[{ bare: "a", pos: 0 }]]]));

    it("ignores the whole line after a comment", () =>
      expect(parse("# comment ; not a command")).toEqual([true, []]));

    it("continues the comment through a folded newline", () =>
      expect(
        parse(String.raw`#a\
                          b`)
      ).toEqual([true, []]));
    it("does not continue the comment through a newline with escaped backslashes", () =>
      expect(
        parse(String.raw`#a\\
b`)
      ).toEqual([true, [[{ bare: "b", pos: 5 }]]]));
    it("continues the comment through a folded newline with escaped backslashes", () =>
      expect(
        parse(String.raw`#a\\\
                          b`)
      ).toEqual([true, []]));
  });

  describe("interpolated words", () => {
    it("can parse a simple word", () =>
      expect(parse("a")).toEqual([true, [[{ bare: "a", pos: 0 }]]]));
    it("can parse a word with non-special punctuation", () =>
      expect(parse("-switch")).toEqual([
        true,
        [[{ bare: "-switch", pos: 0 }]],
      ]));

    it("accepts empty quotes", () =>
      expect(parse('""')).toEqual([true, [[{ text: "", pos: 0 }]]]));
    it("accepts quoted words", () =>
      expect(parse('"a"')).toEqual([true, [[{ text: "a", pos: 0 }]]]));
    it("accepts quoted words with spaces", () =>
      expect(parse('"a b"')).toEqual([true, [[{ text: "a b", pos: 0 }]]]));
    it("allows escaped quotes inside a quote", () =>
      expect(parse('"a\\"b"')).toEqual([true, [[{ text: 'a"b', pos: 0 }]]]));

    it("must close quoted words", () =>
      expect(parse('"a b')).toMatchObject([false, {}]));

    it("does not allow trailing characters after a closing quote", () =>
      expect(parse('""a')).toMatchObject([false, {}]));

    it("accepts escaped spaces", () =>
      expect(parse("a\\ b")).toEqual([true, [[{ text: "a b", pos: 0 }]]]));

    it("treats a non-leading quote as a plain character", () =>
      expect(parse('a"')).toEqual([true, [[{ bare: 'a"', pos: 0 }]]]));
    it("treats a non-leading brace as a plain character", () =>
      expect(parse("a{")).toEqual([true, [[{ bare: "a{", pos: 0 }]]]));
    it("treats an escaped quote as a plain character", () =>
      expect(parse('\\"')).toEqual([true, [[{ text: '"', pos: 0 }]]]));
    it("treats an escaped brace as a plain character", () =>
      expect(parse("\\{")).toEqual([true, [[{ text: "{", pos: 0 }]]]));
    it("treats a quoted brace as a plain character", () =>
      expect(parse('"{"')).toEqual([true, [[{ text: "{", pos: 0 }]]]));
  });

  describe("backslash interpolation", () => {});

  describe("variable interpolation", () => {
    //    variables- bare, brace
  });

  describe("command interpolation", () => {
    it("can parse empty command interpolations", () =>
      expect(parse("[]")).toEqual([true, [[{ pieces: [{ script: [] }] }]]]));
    it("can parse one-word command interpolations", () =>
      expect(parse("[a]")).toEqual([
        true,
        [[{ pieces: [{ script: [[{ bare: "a", pos: 1 }]] }] }]],
      ]));
    it("can parse multi-word command interpolations", () =>
      expect(parse("[a b c]")).toEqual([
        true,
        [
          [
            {
              pieces: [
                {
                  script: [
                    [
                      { bare: "a", pos: 1 },
                      { bare: "b", pos: 3 },
                      { bare: "c", pos: 5 },
                    ],
                  ],
                },
              ],
            },
          ],
        ],
      ]));
    it("can parse multi-command command interpolations", () =>
      expect(parse("[a; b c]")).toEqual([
        true,
        [
          [
            {
              pieces: [
                {
                  script: [
                    [{ bare: "a", pos: 1 }],
                    [
                      { bare: "b", pos: 4 },
                      { bare: "c", pos: 6 },
                    ],
                  ],
                },
              ],
            },
          ],
        ],
      ]));
    it("can parse nested command interpolations", () =>
      expect(parse("[[a]]")).toEqual([
        true,
        [
          [
            {
              pieces: [
                {
                  script: [
                    [{ pieces: [{ script: [[{ bare: "a", pos: 2 }]] }] }],
                  ],
                },
              ],
            },
          ],
        ],
      ]));

    it("can parse pre-word command interpolations", () =>
      expect(parse("[a]b")).toEqual([
        true,
        [
          [
            {
              pieces: [{ script: [[{ bare: "a", pos: 1 }]] }, { bare: "b" }],
            },
          ],
        ],
      ]));
    it("can parse mid-word command interpolations", () =>
      expect(parse("a[b]c")).toEqual([
        true,
        [
          [
            {
              pieces: [
                { bare: "a" },
                { script: [[{ bare: "b", pos: 2 }]] },
                { bare: "c" },
              ],
            },
          ],
        ],
      ]));
    it("can parse end-word command interpolations", () =>
      expect(parse("a[b]")).toEqual([
        true,
        [
          [
            {
              pieces: [{ bare: "a" }, { script: [[{ bare: "b", pos: 2 }]] }],
            },
          ],
        ],
      ]));
    it("can parse multiple command interpolations in a word", () =>
      expect(parse("[a][b]")).toEqual([
        true,
        [
          [
            {
              pieces: [
                { script: [[{ bare: "a", pos: 1 }]] },
                { script: [[{ bare: "b", pos: 4 }]] },
              ],
            },
          ],
        ],
      ]));

    test.each(["[", "a["])(
      "does not permit unterminated commands {%s}",
      (text) => expect(parse(text)).toMatchObject([false, {}])
    );
    test.each(['["]"', '"["]', "[{]}", "{[}]"])(
      "does not permit overlapping commands and quotes {%s}",
      (text) => expect(parse(text)).toMatchObject([false, {}])
    );
  });

  describe("brace words", () => {
    it("can parse empty braces", () =>
      expect(parse("{}")).toEqual([true, [[{ text: "", pos: 0 }]]]));
    it("can parse braces with text", () =>
      expect(parse("{a b c}")).toEqual([true, [[{ text: "a b c", pos: 0 }]]]));
    it("can parse nested braces", () =>
      expect(parse("{{}}")).toEqual([true, [[{ text: "{}", pos: 0 }]]]));
    it("can parse nested braces with text", () =>
      expect(parse("{a{b}c}")).toEqual([true, [[{ text: "a{b}c", pos: 0 }]]]));

    it("must be closed", () =>
      expect(parse("{a b")).toMatchObject([false, {}]));

    it("does not allow trailing characters after a closing brace", () =>
      expect(parse("{}a")).toMatchObject([false, {}]));

    it("doesn't count suppressed braces for nesting", () =>
      expect(parse(String.raw`{a\{b}`)).toEqual([
        true,
        [[{ text: "a\\{b", pos: 0 }]],
      ]));
    it("doesn't count suppressed braces for unnesting", () =>
      expect(parse(String.raw`{a\}b}`)).toEqual([
        true,
        [[{ text: "a\\}b", pos: 0 }]],
      ]));
    it("nests braces after suppressed backslashes", () =>
      expect(parse(String.raw`{a\\{b}}`)).toEqual([
        true,
        [[{ text: "a\\\\{b}", pos: 0 }]],
      ]));

    it("permits newlines in braces", () =>
      expect(parse("{\n}")).toEqual([true, [[{ text: "\n", pos: 0 }]]]));
    it("doesn't fold newlines in braces", () =>
      expect(
        parse(String.raw`{\
}`)
      ).toEqual([true, [[{ text: "\\\n", pos: 0 }]]]));
    it("doesn't fold newlines in braces with escaped backslashes", () =>
      expect(
        parse(String.raw`{\\
}`)
      ).toEqual([true, [[{ text: "\\\\\n", pos: 0 }]]]));
    it("doesn't fold newlines in braces with escaped backslashes", () =>
      expect(
        parse(String.raw`{\\\
}`)
      ).toEqual([true, [[{ text: "\\\\\\\n", pos: 0 }]]]));

    test.each(['{"}"', '"{"}'])(
      "does not permit overlapping braces and quotes {%s}",
      (text) => expect(parse(text)).toMatchObject([false, {}])
    );
  });

  describe("Misc", () => {
    test("Big mess of markup", () => {
      expect(
        parse(String.raw`
    h1 "Hello, World!"
    para [2 + 2]
    block {
        This is a paragraph of text, with one [b bold] word. Yes, this means there has to be some magic in text processing... <b>this</b> won't work.
    }
    block -red "Beware!"
    para "All text should be quoted, it's clearer that way. & blockquotes already should contain paragraphs. (maybe normalize nested paragraphs)"
    block {
        First block
    } {
        Second block

        Is this markdown-parsed?

        [button "No we want to render UI" \\{noop}]
    } {
      Since we want escapes to work, these blocks [i will] be subject to substitutions.
    } 
    # A comment
    para {
      line endings escaped\
      one slash

      not escaped if \\
      two slashes

      escaped with a slash if \\\
      three slashes

      not escaped with two slashes if \\\\
      four slashes

      escaped with two slashes if \\\\\
      five slashes

      not escaped with three slashes if \\\\\\
      six slashes
    }
  `)
      ).toMatchSnapshot();
    });
  });
});
