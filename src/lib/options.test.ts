import { parse } from '../parser';
import { Script, TextPiece } from '../words';
import { getOpt, Options } from './options';

describe("getOpt", () => {
  const expectOpts = <P extends Options>(command: string, opts: P) => {
    const [ok, script] = parse(command);
    expect(ok).toBeTruthy();
    return expect(getOpt((script as Script)[0] as TextPiece[], opts));
  };

  it("parses empty arguments", () => expectOpts("cmd", {}).toEqual([{}]));
  it("parses single positional arguments", () =>
    expectOpts("cmd apple", {}).toEqual([{}, "apple"]));
  it("parses multiple positional arguments", () =>
    expectOpts("cmd apple banana", {}).toEqual([{}, "apple", "banana"]));

  it("enforces minimum # of arguments", () =>
    expectOpts("extrovert", { $min: 1 }).toEqual([
      { error: "extrovert: Not enough arguments" },
    ]));
  it("enforces maximum # of arguments", () =>
    expectOpts("introvert apple banana", { $max: 1 }).toEqual([
      { error: "introvert: Too many arguments" },
    ]));
  it("allows # of arguments in-spec", () =>
    expectOpts("cmd apple", { $min: 1, $max: 1 }).toEqual([{}, "apple"]));

  it("parses boolean switches", () =>
    expectOpts("cmd -red", { red: 0, blue: 0 }).toEqual([
      { red: true, blue: false },
    ]));
  it("parses switches that take arguments", () =>
    expectOpts("cmd -onPepper {sneeze}", { onPepper: 1 }).toEqual([
      { onPepper: ["sneeze"] },
    ]));
  it("enforces switch arguments exist", () =>
    expectOpts("cmd -onPepper", { onPepper: 1 }).toEqual([
      { error: "cmd: Not enough arguments to -onPepper" },
    ]));
  it("raises an error on unknown switches", () =>
    expectOpts("cmd -unsolicited", {}).toEqual([
      { error: "cmd: -unsolicited is not a switch this command knows about" },
    ]));
  it("doesn't count quoted words as switches", () =>
    expectOpts('cmd "-purple"', { purple: 0 }).toEqual([
      { purple: false },
      "-purple",
    ]));

  it("doesn't count switches as positional arguments", () =>
    expectOpts("cmd -yellow banana", { $min: 1, $max: 1, yellow: 0 }).toEqual([
      { yellow: true },
      "banana",
    ]));
  it("can mix positional and switch arguments", () =>
    expectOpts("cmd early -lunch noon evening", { lunch: 1 }).toEqual([
      { lunch: ["noon"] },
      "early",
      "evening",
    ]));
});
