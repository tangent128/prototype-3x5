import { Vm } from '../vm';
import { ProcResult, TextPiece } from '../words';
import { getOpt } from './options';

/**
 * Basic unit of information, also an "actor" in the programming system
 */
export type Card = {
  /** Unique identifier */
  id: number;
  /** Key-value properties on the card */
  fields: Record<string, string>;
  /** Eventually: a markdown string containing code, but for now, just code */
  code: string;
};

export type CardContext = {
  card: Card;
};

export type CardVm = Vm<CardContext>;

export function GetField(vm: CardVm, argv: TextPiece[]): ProcResult {
  return getOpt(argv, { $min: 1, $max: 1 }, ({}, fieldName) => ({
    text: vm.card.fields[fieldName] ?? "",
  }));
}
