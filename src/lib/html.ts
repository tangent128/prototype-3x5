import { TemplateBlock } from '../parser';
import { evaluateWord } from '../vm';
import { AsHtml, Concat, ProcResult, TextPiece } from '../words';
import { CardVm } from './card';
import { getOptRaw } from './options';

const htmlBlockCmd =
  (tag: string) =>
  (vm: CardVm, argv: TextPiece[]): ProcResult =>
    getOptRaw(argv, { $min: 1, raw: 0 }, ({ raw }, words) => {
      function interpolate(word: TextPiece) {
        if ("text" in word && !raw) {
          const [tmplMatch] = TemplateBlock.match(word.text, 0);
          if (tmplMatch) {
            const result = evaluateWord(vm, tmplMatch[0]);
            return AsHtml(result);
          } else {
            return AsHtml(word);
          }
        } else {
          return AsHtml(word);
        }
      }
      return (
        words
          .map((word) => ({ html: `<${tag}>${interpolate(word)}</${tag}>` }))
          .reduce(Concat, null) ?? { html: "" }
      );
    });

const htmlInlineCmd =
  (tag: string) =>
  ({}, argv: TextPiece[]): ProcResult =>
    getOptRaw(argv, { $min: 1 }, (_opts, words) => {
      return {
        html: `<${tag}>${words.map((word) => AsHtml(word)).join(" ")}</${tag}>`,
      };
    });

export const h1 = htmlInlineCmd("h1");
export const para = htmlBlockCmd("p");
export const block = htmlBlockCmd("blockquote");
export const b = htmlInlineCmd("b");
export const i = htmlInlineCmd("i");
export const u = htmlInlineCmd("u");

export const ALL = {
  h1,
  para,
  block,
  b,
  i,
  u,
};
