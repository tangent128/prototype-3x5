import { WordPattern } from '../parser';
import { ProcResult, TextPiece } from '../words';
import { CardVm } from './card';
import { getOpt } from './options';

export function Here(vm: CardVm, argv: TextPiece[]): ProcResult {
  return getOpt(argv, { $max: 0 }, ({}) => ({
    html:
      "pos" in argv[0] && argv[0].pos != undefined
        ? `<b data-pos="${argv[0].pos}">${argv[0].pos}</b>`
        : "<b>???</b>",
  }));
}

export function RegisterJumpHere(textarea: HTMLTextAreaElement) {
  document.body.addEventListener(
    "click",
    (evt) => {
      const dataPos = (evt.target as HTMLElement)?.getAttribute?.("data-pos");
      if (dataPos !== null) {
        const start = Number(dataPos);
        const [match] = WordPattern.match(textarea.value, start);
        if (match) {
          textarea.focus();
          textarea.setSelectionRange(start, match[1]);
        }
      }
    },
    { passive: true }
  );
}

export function Alert(vm: CardVm, argv: TextPiece[]): ProcResult {
  return getOpt(argv, { $min: 1, $max: 1 }, ({}, message) => {
    if (vm.mode[0] == "action") {
      alert(message);
      return { text: message };
    } else {
      return {
        error: "alert: You can only call alert inside an event handler",
      };
    }
  });
}

export const ALL = {
  here: Here,
  alert: Alert,
};
