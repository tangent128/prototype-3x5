import { parse } from '../parser';
import { Proc, runNoctl } from '../vm';
import { AsHtml, AsText, ProcResult, SourcePos, TextPiece } from '../words';
import { CardContext } from './card';
import { getOptRaw } from './options';

export const Button: Proc<CardContext> = (vm, argv: TextPiece[]): ProcResult =>
  getOptRaw(
    argv,
    { $min: 1, $max: 1, onClick: 1 },
    ({ onClick: [onClick = null] }, [label]) => {
      let buttonTrigger = "disabled";
      if (onClick && "pos" in onClick && onClick.pos != undefined) {
        buttonTrigger = `data-notcl-button="${onClick.pos}"`;

        if (vm.mode[0] == "findingAction" && vm.mode[1] == onClick.pos) {
          // handle event!
          const script = parse(AsText(onClick));

          if (script[0]) {
            vm.mode = ["action"];
            const result = runNoctl(vm, script[1], (word) =>
              console.debug(word)
            );
            vm.mode = ["findingAction", onClick.pos];
            return result;
          } else {
            return { error: script[1] };
          }
        }
      }

      return {
        html: `<button ${buttonTrigger}>${AsHtml(label)}</button>`,
      };
    }
  );

export function RegisterButtonOnClick(
  triggerDispatch: (pos: SourcePos) => void
) {
  document.body.addEventListener(
    "click",
    (evt) => {
      const dataNoctlButton = (evt.target as HTMLElement)?.getAttribute?.(
        "data-notcl-button"
      );
      if (dataNoctlButton !== null) {
        const handlerPos = Number(dataNoctlButton);
        triggerDispatch(handlerPos);
      }
    },
    { passive: true }
  );
}
