import { AsText, ErrorResult, ProcResult, TextPiece } from '../words';

export type Options = Record<string, number> & {
  $min?: number;
  $max?: number;
};
type ParsedOptions<P extends Options, V extends string | TextPiece> = {
  [K in keyof P]: K extends "$min" | "$max"
    ? never
    : P[K] extends 0
    ? boolean
    : V[] | [];
} & {
  error?: string;
};

export function getOpt<const P extends Options>(
  argv: TextPiece[],
  options: P,
  procBody: (
    switches: ParsedOptions<P, string>,
    ...argv: string[]
  ) => ProcResult
): ProcResult;
export function getOpt<P extends Options>(
  argv: TextPiece[],
  options: P
): [ParsedOptions<P, string>, ...string[]];
export function getOpt<P extends Options>(
  argv: TextPiece[],
  options: P,
  procBody?: (
    switches: ParsedOptions<P, string>,
    ...argv: string[]
  ) => ProcResult
): [ParsedOptions<P, string>, ...string[]] | ProcResult {
  const [flags, textPieces] = getOptCore(argv, options);

  if ("error" in flags) {
    if (procBody) {
      return flags as ErrorResult;
    } else {
      return [flags as ParsedOptions<P, string>];
    }
  }

  const simpleFlags = Object.fromEntries(
    Object.entries(flags).map(([name, textPieces]) => [
      name,
      typeof textPieces == "boolean"
        ? textPieces
        : (textPieces as TextPiece[]).map(AsText),
    ])
  ) as ParsedOptions<P, string>;

  if (procBody) {
    return procBody(simpleFlags, ...textPieces.map(AsText));
  } else {
    return [simpleFlags, ...textPieces.map(AsText)];
  }
}

export function getOptRaw<const P extends Options>(
  argv: TextPiece[],
  options: P,
  procBody: (
    switches: ParsedOptions<P, TextPiece>,
    argv: TextPiece[]
  ) => ProcResult
): ProcResult;
export function getOptRaw<P extends Options>(
  argv: TextPiece[],
  options: P
): [ParsedOptions<P, TextPiece>, TextPiece[]];
export function getOptRaw<P extends Options>(
  argv: TextPiece[],
  options: P,
  procBody?: (
    switches: ParsedOptions<P, TextPiece>,
    argv: TextPiece[]
  ) => ProcResult
): [ParsedOptions<P, TextPiece>, TextPiece[]] | ProcResult {
  const [flags, textPieces] = getOptCore(argv, options);

  if (procBody) {
    if ("error" in flags) {
      return flags as ErrorResult;
    } else {
      return procBody(flags, textPieces);
    }
  } else {
    return [flags, textPieces];
  }
}

const SWITCH_REGEX = /^-([^]*)/;

function getOptCore<const P extends Options>(
  argv: TextPiece[],
  options: P
): [ParsedOptions<P, TextPiece>, TextPiece[]] {
  const [cmd, ...textPieces] = argv;
  const positionalArgs: TextPiece[] = [];
  const flags: ParsedOptions<P, TextPiece> = Object.fromEntries(
    Object.entries(options)
      .filter(([name]) => name != "$min" && name != "$max")
      .map(([name]) => [name, options[name] == 0 ? false : []])
  ) as ParsedOptions<P, TextPiece>;

  // loop over args & extract switches
  for (let i = 0; i < textPieces.length; i++) {
    const word = textPieces[i];
    if (!("bare" in word)) {
      positionalArgs.push(word);
      continue;
    }

    const switchMatch = SWITCH_REGEX.exec(word.bare);
    if (switchMatch == null) {
      positionalArgs.push(word);
      continue;
    }

    const switchName = switchMatch[1] as keyof ParsedOptions<P, TextPiece> &
      string;
    if (!(switchName in flags)) {
      return [
        {
          error: `${AsText(
            cmd
          )}: -${switchName} is not a switch this command knows about`,
        } as ParsedOptions<P, TextPiece>,
        [],
      ];
    }

    const switchArgCount = options[switchName];
    if (switchArgCount == 0) {
      (flags[switchName] as boolean) = true;
    } else if (i + switchArgCount >= textPieces.length) {
      return [
        {
          error: `${AsText(cmd)}: Not enough arguments to -${switchName}`,
        } as ParsedOptions<P, TextPiece>,
        [],
      ];
    } else {
      const takeUntil = i + switchArgCount;
      for (; i < takeUntil; i++) {
        (flags[switchName] as TextPiece[]).push(textPieces[i + 1]);
      }
    }
  }

  // check correct number of positional arguments
  if (options.$min !== undefined && options.$min > positionalArgs.length) {
    return [
      {
        error: `${AsText(cmd)}: Not enough arguments`,
      } as ParsedOptions<P, TextPiece>,
      [],
    ];
  }
  if (options.$max !== undefined && options.$max < positionalArgs.length) {
    return [
      {
        error: `${AsText(cmd)}: Too many arguments`,
      } as ParsedOptions<P, TextPiece>,
      [],
    ];
  }

  return [flags, positionalArgs];
}
