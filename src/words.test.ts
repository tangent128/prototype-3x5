import { AsHtml, AsText, Concat } from './words';

describe("Text Words", () => {
  test.each([
    [{ bare: "apple" }, "apple"],
    [{ bare: "<pie>" }, "<pie>"],
    [{ text: "banana" }, "banana"],
    [{ text: "<b>kiwi" }, "<b>kiwi"],
    [{ html: "<b>cherry</b>" }, "<b>cherry</b>"],
  ])("AsText(%s)", (word, expected) => expect(AsText(word)).toEqual(expected));

  test.each([
    [{ bare: "apple" }, "apple"],
    [{ bare: "<pie>" }, "&lt;pie&gt;"],
    [{ text: "banana" }, "banana"],
    [{ text: "<b>kiwi" }, "&lt;b&gt;kiwi"],
    [{ html: "<b>cherry</b>" }, "<b>cherry</b>"],
  ])("AsHtml(%s)", (word, expected) => expect(AsHtml(word)).toEqual(expected));

  test.each([
    [null, { bare: "1" }, { text: "1" }],
    [null, { bare: ">1" }, { text: ">1" }],
    [null, { text: "2" }, { text: "2" }],
    [null, { text: "<b>3</b>" }, { text: "<b>3</b>" }],
    [null, { html: "2" }, { html: "2" }],
    [null, { html: "<b>3</b>" }, { html: "<b>3</b>" }],
    [{ bare: "&pple" }, { bare: "1" }, { text: "&pple1" }],
    [{ bare: "&pple" }, { bare: ">1" }, { text: "&pple>1" }],
    [{ bare: "&pple" }, { text: "2" }, { text: "&pple2" }],
    [{ bare: "&pple" }, { text: "<b>3</b>" }, { text: "&pple<b>3</b>" }],
    [{ bare: "&pple" }, { html: "2" }, { html: "&amp;pple2" }],
    [{ bare: "&pple" }, { html: "<b>3</b>" }, { html: "&amp;pple<b>3</b>" }],
    [{ text: "<b>anana" }, { bare: "1" }, { text: "<b>anana1" }],
    [{ text: "<b>anana" }, { bare: ">1" }, { text: "<b>anana>1" }],
    [{ text: "<b>anana" }, { text: "2" }, { text: "<b>anana2" }],
    [{ text: "<b>anana" }, { text: "<b>3</b>" }, { text: "<b>anana<b>3</b>" }],
    [{ text: "<b>anana" }, { html: "2" }, { html: "&lt;b&gt;anana2" }],
    [
      { text: "<b>anana" },
      { html: "<b>3</b>" },
      { html: "&lt;b&gt;anana<b>3</b>" },
    ],
    [{ html: "<img />" }, { bare: "1" }, { html: "<img />1" }],
    [{ html: "<img />" }, { bare: ">1" }, { html: "<img />&gt;1" }],
    [{ html: "<img />" }, { text: "2" }, { html: "<img />2" }],
    [
      { html: "<img />" },
      { text: "<b>3</b>" },
      { html: "<img />&lt;b&gt;3&lt;/b&gt;" },
    ],
    [{ html: "<img />" }, { html: "2" }, { html: "<img />2" }],
    [{ html: "<img />" }, { html: "<b>3</b>" }, { html: "<img /><b>3</b>" }],
  ])("Concat(%s, %s)", (left, right, expected) =>
    expect(Concat(left, right)).toEqual(expected)
  );

  test.each([
    [null, { bare: "1", pos: 13 }, { text: "1", pos: 13 }],
    [null, { text: "2", pos: 13 }, { text: "2", pos: 13 }],
    [null, { html: "3", pos: 13 }, { html: "3", pos: 13 }],
    [{ bare: "4", pos: 13 }, { bare: "5", pos: 15 }, { text: "45" }],
    [{ html: "6", pos: 13 }, { html: "7", pos: 15 }, { html: "67" }],
  ])(
    "Concat(%s, %s) strips source position when combining words",
    (left, right, expected) => expect(Concat(left, right)).toEqual(expected)
  );
});
